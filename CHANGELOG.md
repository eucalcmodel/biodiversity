# Changelog

## 1.9
### Changes
- Transfer interface BDY-AGR to BDY-LUS
    - Node 4.5 Biodiversity: rename output port 2 to 4.1 Land use
    - Update interface L2: switch from 4.3 Agriculture to 4.1 Land-use
- DataWriter: add "biodiversity" as possible Module choice
- Interface to TPE: Remove columns no longer necessary and/or passed trough 1.4 Climate emissions.

### Removes
- Conversion of raw GHG emissions to CO2e now passed over to 1.4 Climate emissions

## 1.8
### Added
- Node BDY_biodiversity
- Calculation of CO2e in main node 4.5 Biodiversity

### Changes

### Removed
- Node BDY_emissions
- Node BDY_area

### Fixed
- Data files and lever naming

## 1.6
### Added
- Interface to 1.4 Climate Emissions

### Changes
- Level selection add one port for emissions
- Node *Merge ots frozen-land and area* add one port for emissions
- Node *4.5 Biodiversity*
  - new out-port to 1.4 Climate Emissions
- Update of interface to pathway explorer: needs to be incorporated in L2

### Removes

### Fixed
- Frozen area data

## 1.5
### Added
- Import area data
- Import frozen area data
- Interface to Agriculture

### Changes

### Removes

### Fixed

## 1.0
- Initial version, integrating all modules
